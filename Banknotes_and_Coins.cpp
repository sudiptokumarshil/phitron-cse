
// #include <bits/stdc++.h>

// using namespace std;

// class Note
// {

// public:
//     int times;
//     double note;

//     Note(int times, double note)
//     {
//         this->times = times;
//         this->note = note;
//     }
// };

// class Coin
// {

// public:
//     int times;
//     double note;

//     Coin(int times, double note)
//     {
//         this->times = times;
//         this->note = note;
//     }
// };

// int main()
// {
//     vector<double> note = {100.00, 50.00, 20.00, 10.00, 5.00, 2.00};
//     vector<double> coin = {1.00, 0.50, 0.25, 0.10, 0.05, 0.01};
//     vector<Note> note_result;
//     vector<Coin> coin_result;

//     double n;
//     cin >> n;

//     double temp = n;

//     for (int i = 0; i < note.size(); i++)
//     {
//         int something = temp / note[i];
//         note_result.push_back(Note(something, note[i]));
//         temp = fmod(temp, note[i]);
//     }

//     for (int i = 0; i < coin.size(); i++)
//     {
//         int something = temp / coin[i];
//         coin_result.push_back(Coin(something, coin[i]));
//         temp = fmod(temp, coin[i]);
//     }

//     cout << "NOTAS:" << endl;
//     for (Note val : note_result)
//     {
//         cout << val.times << " nota(s) de R$ " << fixed << setprecision(2) << val.note << endl;
//     }

//     cout << "MOEDAS:" << endl;
//     for (Coin val : coin_result)
//     {
//         cout << val.times << " moeda(s) de R$ " << fixed << setprecision(2) << val.note << endl;
//     }

//     return 0;
// }

#include <bits/stdc++.h>

using namespace std;

class Note
{
public:
    int times;
    int note;

    Note(int times, int note)
    {
        this->times = times;
        this->note = note;
    }
};

class Coin
{
public:
    int times;
    int note;

    Coin(int times, int note)
    {
        this->times = times;
        this->note = note;
    }
};

int main()
{
    vector<int> note = {10000, 5000, 2000, 1000, 500, 200}; // Values in cents
    vector<int> coin = {100, 50, 25, 10, 5, 1};             // Values in cents
    vector<Note> note_result;
    vector<Coin> coin_result;

    double n;
    cin >> n;

    int temp = static_cast<int>(n * 100); // Convert to cents

    for (int i = 0; i < note.size(); i++)
    {
        int something = temp / note[i];
        note_result.push_back(Note(something, note[i]));
        temp = temp % note[i];
    }

    for (int i = 0; i < coin.size(); i++)
    {
        int something = temp / coin[i];
        coin_result.push_back(Coin(something, coin[i]));
        temp = temp % coin[i];
    }

    cout << "NOTAS:" << endl;
    for (Note val : note_result)
    {
        cout << val.times << " nota(s) de R$ " << fixed << setprecision(2) << val.note / 100.0 << endl; // Convert back to dollars
    }

    cout << "MOEDAS:" << endl;
    for (Coin val : coin_result)
    {
        cout << val.times << " moeda(s) de R$ " << fixed << setprecision(2) << val.note / 100.0 << endl; // Convert back to dollars
    }

    return 0;
}
