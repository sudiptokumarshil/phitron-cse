#include <iostream>
using namespace std;

void init_code() {
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}

int main() {

    init_code();
    
    string firstNumber, secondNumber, result;
    cin >> firstNumber >> secondNumber;
    
    int carry = 0, i = firstNumber.length() - 1, j = secondNumber.length() - 1;
    
    while (i >= 0 || j >= 0 || carry) {
        int sum = carry;
        if (i >= 0) sum += firstNumber[i--] - '0';
        if (j >= 0) sum += secondNumber[j--] - '0';
        
        carry = sum / 10;
        sum %= 10;
        result = to_string(sum) + result;
    }
    
    cout << result << endl;

    return 0;
}
