#include <bits/stdc++.h>

using namespace std;

int main()
{

    int T;
    cin >> T;
    for (int i = 0; i < T; i++)
    {
        int N;
        cin >> N;
        set<int> s;
        vector<int> v;
        while (N--)
        {
            int x;
            cin >> x;
            s.insert(x);
        }

        for (auto it = s.begin(); it != s.end(); it++)
        {
            v.push_back(*it);
        }

        sort(v.begin(), v.end(), greater<int>());
        
        for (int j = 0; j < v.size(); j++)
        {
            cout << v[j] << " ";
        }

        cout << endl;
    }

    return 0;
}