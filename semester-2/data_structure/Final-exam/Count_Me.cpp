// #include <bits/stdc++.h>
// using namespace std;

// int main()
// {
//     int T;
//     cin >> T;
//     cin.ignore();

//     for (int i = 0; i < T; i++)
//     {
//         string sentence;
//         getline(cin, sentence);
//         string word;
//         stringstream ss(sentence);
//         map<string, int> mp;

//         while (ss >> word)
//         {
//             mp[word]++;
//         }

//         int maxCnt = 0;
//         string mostWord;

//         for (auto it = mp.begin(); it != mp.end(); it++)
//         {
//             if (it->second > maxCnt)
//             {
//                 maxCnt = it->second;
//                 mostWord = it->first;
//             }
//         }

//         for (auto it = mp.begin(); it != mp.end(); it++)
//         {
//             if (it->second == maxCnt && it->first != mostWord)
//             {
//                 mostWord = it->first;
//                 break;
//             }
//         }

//         cout << mostWord << " " << maxCnt << endl;
//     }
//     return 0;
// }

#include <bits/stdc++.h>
using namespace std;

int main()
{
    int T;
    cin >> T;
    cin.ignore();

    for (int i = 0; i < T; i++)
    {
        string sentence;
        getline(cin, sentence);
        string word;
        stringstream ss(sentence);
        map<string, int> mp;
        int maxCnt = 0;
        string mostWord;

        while (ss >> word)
        {
            mp[word]++;
            if (mp[word] > maxCnt)
            {
                maxCnt = mp[word];
                mostWord = word;
            }
        }

        cout << mostWord << " " << maxCnt << endl;
    }
    return 0;
}
