#include <bits/stdc++.h>

using namespace std;

int main()
{

    int T;
    cin >> T;

    for (int i = 0; i < T; i++)
    {
        int N;
        cin >> N;
        map<int, int> mp;

        int maxVal = 0;
        int maxCnt = 0;

        for (int j = 0; j < N; j++)
        {
            int val;
            cin >> val;
            mp[val]++;

            if (maxVal < val && mp[val] == maxCnt || mp[val] > maxCnt)
            {
                maxVal = val;
                maxCnt = mp[val];
            }
        }
        cout << maxVal << " " << maxCnt << endl;
    }

    return 0;
}