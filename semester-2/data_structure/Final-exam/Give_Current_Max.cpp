#include <bits/stdc++.h>
using namespace std;

class Student
{
public:
    string name;
    int roll;
    int marks;
    Student(string name, int roll, int marks)
    {
        this->name = name;
        this->roll = roll;
        this->marks = marks;
    }
};

void showMaxMarks(vector<Student> student)
{
    string name;
    int roll;
    int marks = 0;

    for (int i = 0; i < student.size(); i++)
    {
        if (student[i].marks > marks || student[i].marks == marks && student[i].roll < roll)
        {
            name = student[i].name;
            roll = student[i].roll;
            marks = student[i].marks;
        }
    }

    cout << name << " " << roll << " " << marks << endl;
}

int returnIndex(vector<Student> student)
{
    int marks = 0;
    int index = 0;

    for (int i = 0; i < student.size(); i++)
    {
        if (student[i].marks > marks || student[i].marks == marks && student[i].roll < student[index].roll)
        {
            marks = student[i].marks;
            index = i;
        }
    }
    return index;
}

int main()
{
    int N;
    cin >> N;

    vector<Student> st;

    for (int i = 0; i < N; i++)
    {
        string name;
        int roll, marks;
        cin >> name >> roll >> marks;

        st.push_back(Student(name, roll, marks));
    }

    int Q;
    cin >> Q;

    for (int i = 0; i < Q; i++)
    {
        int c;
        cin >> c;

        if (c == 0)
        {
            string name;
            int roll, marks;
            cin >> name >> roll >> marks;

            st.push_back(Student(name, roll, marks));
            showMaxMarks(st);
        }
        else if (c == 1)
        {
            if (st.size() == 0)
                cout << "Empty" << endl;
            else
                showMaxMarks(st);
        }
        else if (c == 2)
        {
            if (st.size() == 0)
            {
                cout << "Empty" << endl;
            }
            else
            {
                st.erase(st.begin() + returnIndex(st));
                if (st.size() == 0)
                    cout << "Empty" << endl;
                else
                    showMaxMarks(st);
            }
        }
        else
        {
            break;
        }
    }

    return 0;
}
