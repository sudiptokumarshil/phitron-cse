#include <bits/stdc++.h>

using namespace std;

class Node
{
    public:
        int val;
        Node *next;
        Node(int val)
        {
            this->val = val;
            this->next = NULL;
        }
    };

int main()
{

    Node *head = new Node(100);
    Node *a = new Node(200);

    head->next = a;

    // printing linked list.

    cout << head->val << endl;
    cout << head->next->val << endl;
    cout << head->next << endl;

    return 0;
}