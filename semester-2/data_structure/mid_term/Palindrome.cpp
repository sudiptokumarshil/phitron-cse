#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_node_at_tail(Node *&head, Node *&tail, int val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        tail = newNode;
        return;
    }
    tail->next = newNode;
    tail = tail->next;
}

void reverse_list(Node *&head, Node *cur)
{
    if (cur->next == NULL)
    {
        head = cur;
        return;
    }
    reverse_list(head, cur->next);
    cur->next->next = cur;
    cur->next = NULL;
}

bool check_palindrome(Node *head, Node *head2)
{
    Node *temp = head;
    Node *temp2 = head2;

    while (temp != NULL)
    {
        if (temp->val != temp2->val)
        {
            return false;
        }
        temp = temp->next;
        temp2 = temp2->next;
    }
    return true;
}

int main()
{
    Node *head = NULL;
    Node *tail = NULL;

    Node *head2 = NULL;
    Node *tail2 = NULL;

    while (true)
    {
        int val;
        cin >> val;
        if (val == -1)
            break;
        insert_node_at_tail(head, tail, val);
        insert_node_at_tail(head2, tail2, val);
    }

    reverse_list(head2, head2);

    if (check_palindrome(head, head2) == true)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return 0;
}