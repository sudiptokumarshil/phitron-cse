#include <bits/stdc++.h>

using namespace std;
class Node
{
public:
    string val;
    Node *next;
    Node *prev;
    Node(string val)
    {
        this->val = val;
        this->next = NULL;
        this->prev = NULL;
    }
};

void insert_at_tail(Node *&head, Node *&tail, string val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        tail = newNode;
        return;
    }
    tail->next = newNode;
    newNode->prev = tail;
    tail = tail->next;
}

bool findVal(Node *head, string value)
{
    Node *current = head;
    while (current != NULL)
    {
        if (current->val == value)
        {
            return true;
        }
        current = current->next;
    }
    return false;
}

int main()
{

    Node *head = NULL;
    Node *tail = NULL;

    while (true)
    {
        string val;
        cin >> val;
        if (val == "end")
            break;
        else
        {
            insert_at_tail(head, tail, val);
        }
    }

    int Q;
    cin >> Q;

    Node *currentHead = head;

    for (int i = 0; i < Q; i++)
    {
        string question;
        cin >> question;

        if (question == "visit")
        {
            string address;
            cin >> address;

            Node *temp = currentHead;
            bool foundflag = false;

            for (Node *i = head; i->next != NULL; i = i->next)
            {
                if (i->val == address)
                {
                    foundflag = true;
                    currentHead = i->next;
                    break;
                }
            }

            if (foundflag == false)
            {
                cout << "Not Available" << endl;
            }
        }
        else if (question == "prev")
        {
            if (currentHead->prev != NULL)
            {
                cout << currentHead->prev->val << endl;
                currentHead = currentHead->prev;
            }
            else
            {
                cout << "Not Available" << endl;
            }
        }
        else if (question == "next")
        {
            if (currentHead->next != NULL)
            {
                cout << currentHead->next->val << endl;
                currentHead = currentHead->next;
            }
            else
            {
                cout << "Not Available" << endl;
            }
        }
    }
    return 0;
}