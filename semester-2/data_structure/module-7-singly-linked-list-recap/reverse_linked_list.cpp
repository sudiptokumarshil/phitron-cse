#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_at_tail(Node *&head, int val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        return;
    }
    Node *temp = head;

    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = newNode;
}

void print_recursion(Node *head)
{
    if (head == NULL)
        return;
    cout << head->val << " ";
    print_recursion(head->next);
}

void print_recursion_with_reverse(Node *head)
{
    if (head == NULL)
        return;

    print_recursion_with_reverse(head->next);
    cout << head->val << " ";
}

int main()
{

    Node *head = NULL;
    int step;
    cin >> step;
    for (int i = 0; i < step; i++)
    {
        int val;
        cin >> val;
        insert_at_tail(head, val);
    }

    // Normal Print ...
    print_recursion(head);

    // Reverse Print ...
    cout << endl
         << endl;
    print_recursion_with_reverse(head);

    return 0;
}