#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_node_at_tail(Node *&head, int val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        return;
    }
    Node *temp = head;
    while (temp->next != NULL)
    {

        temp = temp->next;
    }
    temp->next = newNode;
}

void print_linked_list(Node *head)
{
    Node *temp = head;

    while (temp != NULL)
    {
        cout << temp->val << " ";
        temp = temp->next;
    }
}

void print_middle(Node *head)
{
    Node *tmp = head;

    int countNode = 0;
    while (tmp != NULL)
    {
        countNode = countNode + 1;
        tmp = tmp->next;
    }

    Node *temp = head;
    if (countNode % 2 == 0)
    {
        int first = countNode / 2;

        for (int i = 0; i < first - 1; i++)
        {
            temp = temp->next;
        }
        cout << temp->val << " " << temp->next->val << endl;
    }
    else
    {
        int middle = (countNode + 1) / 2;
        for (int i = 0; i < middle - 1; i++)
        {
            temp = temp->next;
        }
        cout << temp->val << endl;
    }
}

int main()
{

    Node *head = NULL;

    while (true)
    {
        int val;
        cin >> val;
        if (val == -1)
            break;
        insert_node_at_tail(head, val);
    }

    for (Node *i = head; i->next != NULL; i = i->next)
    {
        for (Node *j = i->next; j != NULL; j = j->next)
        {
            if (i->val < j->val)
            {
                swap(i->val, j->val);
            }
        }
    }

    print_middle(head);

    return 0;
}