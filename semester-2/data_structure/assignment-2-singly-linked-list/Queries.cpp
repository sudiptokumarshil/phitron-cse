#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_at_head(Node *&head, int val)
{

    Node *newNode = new Node(val);
    Node *newNode2 = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        head->next = newNode2;
        return;
    }
    head->val = val;
}

void insert_at_tail(Node *&head, int val)
{
    Node *newNode = new Node(val);
    Node *newNode2 = new Node(val);

    if (head == NULL)
    {
        head = newNode;
        head->next = newNode2;
        return;
    }
    else if (head->next == NULL)
    {
        head->next = newNode;
        return;
    }
    head->next->val = val;
}

void print_head_tail(Node *head)
{
    if (head == NULL)
    {
    }
    else if (head->next != NULL)
    {
        cout << head->val << " " << head->next->val << endl;
    }
    else
    {
        cout << head->val << " " << head->val << endl;
    }
}

int main()
{
    Node *head = NULL;

    int Q;
    cin >> Q;
    for (int i = 0; i < Q; i++)
    {

        int X, V;
        cin >> X >> V;

        if (X == 0)
        {
            insert_at_head(head, V);
        }
        else if (X == 1)
        {
            insert_at_tail(head, V);
        }
        print_head_tail(head);
    }

    return 0;
}