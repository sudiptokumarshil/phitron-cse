#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_node_at_tail(Node *&head, Node *&tail, int val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        tail = newNode;
        return;
    }
    tail->next = newNode;
    tail = newNode;
}

void print_original_with_recursion(Node *head)
{
    if (head == NULL)
        return;
    cout << head->val << " ";
    print_original_with_recursion(head->next);
}

void recursion_with_reverse(Node *head)
{
    if (head == NULL)
        return;

    recursion_with_reverse(head->next);
    cout << head->val << " ";
}

int main()
{

    Node *head = NULL;
    Node *tail = NULL;

    while (true)
    {
        int val;
        cin >> val;
        if (val == -1)
            break;
        insert_node_at_tail(head, tail, val);
    }

    recursion_with_reverse(head);
    cout << endl;
    print_original_with_recursion(head);

    return 0;
}