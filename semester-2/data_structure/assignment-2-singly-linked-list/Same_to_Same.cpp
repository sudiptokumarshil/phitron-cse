#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_at_tail(Node *&head, int val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        return;
    }
    Node *temp = head;

    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = newNode;
}

int check_same(Node *head, Node *secondHead)
{
    Node *temp = head;
    Node *temp2 = secondHead;
    int flag = 1;

    while (true)
    {
        if (temp != NULL && temp2 != NULL)
        {
            if (temp->val == temp2->val)
            {
                temp = temp->next;
                temp2 = temp2->next;
            }
            else
            {
                flag = 2;
                break;
            }
        }
        else if (temp == NULL && temp2 != NULL)
        {
            flag = 2;
            break;
        }
        else if (temp != NULL && temp2 == NULL)
        {
            flag = 2;
            break;
        }
        else
        {
            break;
        }
    }

    return flag;
}

int main()
{

    Node *head = NULL;
    Node *secondHead = NULL;

    while (true)
    {
        int val;
        cin >> val;
        if (val == -1)
            break;
        insert_at_tail(head, val);
    }

    while (true)
    {
        int val;
        cin >> val;
        if (val == -1)
            break;
        insert_at_tail(secondHead, val);
    }

    if (check_same(head, secondHead) == 1)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return 0;
}