#include <bits/stdc++.h>

using namespace std;

int main()
{
    stack<int> st;
    queue<int> que;

    vector<long long int> stackList;
    vector<long long int> queueList;

    long long int N, M;
    cin >> N >> M;

    for (int i = 0; i < N; i++)
    {
        int x;
        cin >> x;
        st.push(x);
    }

    while (!st.empty())
    {
        stackList.push_back(st.top());
        st.pop();
    }

    for (int i = 0; i < M; i++)
    {
        int x;
        cin >> x;
        que.push(x);
    }

    while (!que.empty())
    {
        queueList.push_back(que.front());
        que.pop();
    }

    if (stackList.size() != queueList.size())
    {
        cout << "NO" << endl;
        return 0;
    }

    bool flag = true;
    for (int i = 0; i < stackList.size(); i++)
    {
        if (stackList[i] != queueList[i])
        {
            flag = false;
        }
    }

    if (flag == true)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return 0;
}