#include <bits/stdc++.h>

using namespace std;

int main()
{

    queue<string> person;

    int Q;
    cin >> Q;

    for (int i = 0; i < Q; i++)
    {
        int query;
        string name;
        cin >> query;
        if (query == 0)
        {
            cin >> name;
            person.push(name);
        }
        else
        {   
            if (!person.empty())
            {
                cout << person.front() << endl;
                person.pop();
            }else {
                cout << "Invalid" << endl;
            }
            
        }
    }

    return 0;
}