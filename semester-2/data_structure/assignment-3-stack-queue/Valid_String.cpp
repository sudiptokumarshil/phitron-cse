#include <bits/stdc++.h>

using namespace std;

int main()
{

    int T;
    cin >> T;

    for (int i = 0; i < T; i++)
    {
        string S;
        cin >> S;
        queue <char> que;

        for (int j = 0; j < S.size(); j++)
        {
            if (!que.empty() && que.front() != S[j])
            {
                que.pop();
            }
            else
            {
                que.push(S[j]);
            }
        }

        if (que.empty() == true)
        {
            cout << "YES" << endl;
        }
        else
        {
            cout << "NO" << endl;
        }
    }

    return 0;
}