#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node *prev;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
        this->prev = NULL;
    }
};
class myStack
{
public:
    Node *head = NULL;
    Node *tail = NULL;
    int countSize = 0;

    void push(int val)
    {
        countSize++;

        Node *newNode = new Node(val);
        if (head == NULL)
        {
            head = newNode;
            tail = newNode;
            return;
        }
        newNode->prev = tail;
        tail->next = newNode;
        tail = tail->next;
    }

    void pop()
    {
        countSize--;

        Node *deleteNode = tail;
        tail = tail->prev;
        if (tail == NULL)
            head = NULL;
        delete deleteNode;
    }

    int top()
    {
        return tail->val;
    }

    int size()
    {
        return countSize;
    }

    bool empty()
    {
        if (countSize == 0)
            return true;
        else
            return false;
    }
};

class myQueue
{

public:
    Node *head = NULL;
    Node *tail = NULL;

    int countSize = 0;
    void push(int val)
    {
        countSize++;

        Node *newNode = new Node(val);
        if (head == NULL)
        {
            head = newNode;
            tail = newNode;
            return;
        }
        newNode->prev = tail;
        tail->next = newNode;
        tail = tail->next;
    }

    void pop()
    {
        countSize--;

        Node *deleteNode = head;
        head = head->next;
        if (head == NULL)
        {
            tail = NULL;
            delete deleteNode;
            return;
        }
        head->prev = NULL;
    }

    int front()
    {
        return head->val;
    }

    int size()
    {
        return countSize;
    }

    bool empty()
    {
        if (countSize == 0)
            return true;
        else
            return false;
    }
};

int main()
{

    vector<long long int> stackList;
    vector<long long int> queueList;

    myStack st;
    myQueue que;

    long long int N, M;
    cin >> N >> M;

    for (int i = 0; i < N; i++)
    {
        int x;
        cin >> x;
        st.push(x);
    }

    while (!st.empty())
    {
        stackList.push_back(st.top());
        st.pop();
    }

    for (int i = 0; i < M; i++)
    {
        int x;
        cin >> x;
        que.push(x);
    }

    while (!que.empty())
    {
        queueList.push_back(que.front());
        que.pop();
    }

    if (stackList.size() != queueList.size())
    {
        cout << "NO" << endl;
        return 0;
    }

    bool flag = true;
    for (int i = 0; i < stackList.size(); i++)
    {
        if (stackList[i] != queueList[i])
        {
            flag = false;
        }
    }

    if (flag == true)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return 0;
}