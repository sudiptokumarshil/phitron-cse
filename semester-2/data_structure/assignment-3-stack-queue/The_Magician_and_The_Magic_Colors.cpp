#include <bits/stdc++.h>

using namespace std;

int main()
{

    int T;
    cin >> T;

    for (int i = 0; i < T; i++)
    {
        int N;
        cin >> N;
        char S[N];

        queue<char> que;

        for (int j = 0; j < N; j++)
        {
            cin >> S[j];
        }

        for (int k = 0; k < N; k++)
        {
            if (!que.empty())
            {
                if ((que.front() == 'R' && S[k] == 'B') || (que.front() == 'B' && S[k] == 'R'))
                {
                    que.pop();
                    que.push('P');
                }
                else if ((que.front() == 'R' && S[k] == 'G') || (que.front() == 'G' && S[k] == 'R'))
                {
                    que.pop();
                    que.push('Y');
                }
                else if ((que.front() == 'B' && S[k] == 'G') || (que.front() == 'G' && S[k] == 'B'))
                {
                    que.pop();
                    que.push('C');
                }
                else if (que.front() == S[k])
                {
                    que.pop();
                }
                else
                {
                    que.push(S[k]);
                }
            }
            else
            {
                que.push(S[k]);
            }
        }

        while (!que.empty())
        {
            cout << que.front();
            que.pop();
        }
        cout << endl;
    }

    return 0;
}