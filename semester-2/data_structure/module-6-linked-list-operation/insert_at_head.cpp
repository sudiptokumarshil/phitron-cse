#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

void insert_at_tail(Node *&head, int val)
{
    Node *newNode = new Node(val);
    if (head == NULL)
    {
        head = newNode;
        return;
    }
    Node *temp = head;

    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = newNode;
}

void insert_at_any_position(Node *&head, int pos, int val)
{
    Node *newNode = new Node(val);
    if (pos <= 0)
    {
        return;
    }
    Node *temp = head;

    for (int i = 0; i < pos - 1; i++)
    {
        temp = temp->next;
    }
    newNode->next = temp->next;
    temp->next = newNode;
}

void print_linked_list(Node *head)
{
    Node *temp = head;
    while (temp != NULL)
    {
        cout << temp->val << " ";
        temp = temp->next;
    }
}

void insert_at_head(Node *&head, int val)
{
    Node *newNode = new Node(val);
    newNode->next = head;
    head = newNode;
}

int main()
{

    int step;
    cin >> step;
    Node *head = NULL;
    for (int i = 0; i < step; i++)
    {
        int val;
        cin >> val;
        insert_at_tail(head, val);
    }
    int pos, val;
    cin >> pos >> val;

    insert_at_any_position(head, pos, val);
    insert_at_head(head,100);
    print_linked_list(head);

    return 0;
}