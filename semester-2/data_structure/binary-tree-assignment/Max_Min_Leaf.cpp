#include <bits/stdc++.h>

using namespace std;

class Node
{

public:
    int val;
    Node *left;
    Node *right;
    Node(int val)
    {
        this->val = val;
        this->left = NULL;
        this->right = NULL;
    }
};

Node *inputTree()
{
    int val;
    cin >> val;
    Node *root;
    if (val == -1)
        root = NULL;
    else
        root = new Node(val);

    queue<Node *> q;
    if (root)
        q.push(root);

    while (!q.empty())
    {
        Node *f = q.front();
        q.pop();

        int l, r;
        cin >> l >> r;
        Node *left;
        Node *right;

        if (l == -1)
            left = NULL;
        else
            left = new Node(l);

        if (r == -1)
            right = NULL;
        else
            right = new Node(r);

        f->left = left;
        f->right = right;

        if (f->left)
            q.push(f->left);
        if (f->right)
            q.push(f->right);
    }
    return root;
}

void leafNode(Node *root, vector<int> &leafList)
{
    if (root == NULL)
        return;

    if (root->left == NULL && root->right == NULL)
    {
        leafList.push_back(root->val);
    }
    else
    {
        leafNode(root->left, leafList);
        leafNode(root->right, leafList);
    }
}

int main()
{

    Node *root = inputTree();
    vector<int> v;
    leafNode(root, v);

    for (int i = 0; i < v.size() - 1; i++)
    {
        for (int j = i + 1; j < v.size(); j++)
        {
            if (v[i] < v[j])
            {
                int temp = v[i];
                v[i] = v[j];
                v[j] = temp;
            }
        }
    }

    cout << v[0] << " " <<v[v.size() - 1];

    return 0;
}