#include <bits/stdc++.h>

using namespace std;

int main()
{
    vector<int> v;
    int n;
    cin >> n;

    for (int i = 0; i < n; i++)
    {
        int x;
        cin >> x;

        v.push_back(x);

        int curIdx = v.size() - 1;
        while (curIdx != 0)
        {
            int parentIdx = (curIdx - 1) / 2;

            if (v[parentIdx] < v[curIdx]) // Here will be chnaged (>) if we want to use min heap.
                swap(v[parentIdx], v[curIdx]);
            else
                break;
            curIdx = parentIdx;
        }
    }

    for (int val : v)
        cout << val << " ";

    return 0;
}