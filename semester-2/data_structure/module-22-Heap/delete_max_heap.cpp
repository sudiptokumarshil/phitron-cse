#include <bits/stdc++.h>

using namespace std;

void insertHeap(vector<int> &v, int x)
{
    v.push_back(x);

    int curIdx = v.size() - 1;
    while (curIdx != 0)
    {
        int parentIdx = (curIdx - 1) / 2;

        if (v[parentIdx] < v[curIdx]) // Here will be chnaged (>) if we want to use min heap.
            swap(v[parentIdx], v[curIdx]);
        else
            break;
        curIdx = parentIdx;
    }
}

void deleteHeap(vector<int> &v)
{
    v[0] = v[v.size() - 1];
    v.pop_back();
    int cur = 0;
    while (true)
    {
        int left_idx = cur * 2 + 1;
        int right_idx = cur * 2 + 2;
        int last_idx = v.size() - 1;
        if (left_idx <= last_idx && right_idx <= last_idx)
        {
            // duitai ase
            if (v[left_idx] >= v[right_idx] && v[left_idx] > v[cur])
            {
                swap(v[left_idx], v[cur]);
                cur = left_idx;
            }
            else if (v[right_idx] >= v[left_idx] && v[right_idx] > v[cur])
            {
                swap(v[right_idx], v[cur]);
                cur = right_idx;
            }
            else
            {
                break;
            }
        }
        else if (left_idx <= last_idx)
        {
            // Left ase.
            if (v[left_idx] > v[cur])
            {
                swap(v[left_idx], v[cur]);
                cur = left_idx;
            }
            else
            {
                break;
            }
        }
        else if (right_idx <= last_idx)
        {
            // Right Ase
            if (v[right_idx] > v[cur])
            {
                swap(v[right_idx], v[cur]);
                cur = right_idx;
            }
            else
            {
                break;
            }
        }
        else
        {

            break;
        }
    }
}

void printHeap(vector<int> v)
{
    for (int val : v)
        cout << val << " ";
    cout << endl;
}

int main()
{
    int n;
    cin >> n;
    vector<int> v;

    for (int i = 0; i < n; i++)
    {
        int x;
        cin >> x;
        insertHeap(v, x);
    }

    printHeap(v);
    deleteHeap(v);
    printHeap(v);
    return 0;
}