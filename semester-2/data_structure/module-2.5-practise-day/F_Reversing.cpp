#include <bits/stdc++.h>

using namespace std;

int main()
{

    int N;
    cin >> N;
    vector <int> v;

    for (int i = 0; i < N; i++)
    {
        int temp;
        cin >> temp;
        v.push_back(temp);
    }

    for (int i = v.size()-1; i >= 0; i--)
    {
        cout << v[i] << " ";
    }

    return 0;
}