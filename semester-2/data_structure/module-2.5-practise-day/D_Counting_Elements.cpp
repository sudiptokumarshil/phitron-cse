#include <bits/stdc++.h>

using namespace std;

int main()
{

    int N;
    cin >> N;

    vector<int> v;
    for (int i = 0; i < N; i++)
    {
        int temp;
        cin >> temp;
        v.push_back(temp);
    }

    int flag = 0;

    for (int i = 0; i < v.size(); i++)
    {
        auto it = find(v.begin(), v.end(), v[i] + 1);
        if (it != v.end())
        {
            ++flag;
        }
    }

    cout << flag << endl;

    return 0;
}