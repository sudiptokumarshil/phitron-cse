#include <bits/stdc++.h>

using namespace std;

int main()
{

    int N;
    cin >> N;
    vector<int> v;

    for (int i = 0; i < N; i++)
    {
        int temp;
        cin >> temp;
        v.push_back(temp);
    }

    for (int i = 0; i < v.size(); i++)
    {
        if (v[i] > 0)
        {
            v[i] = 1;
        }
        else if (v[i] < 0)
        {
            v[i] = 2;
        }

        cout << v[i] << " ";
    }

    return 0;
}