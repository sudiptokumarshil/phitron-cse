#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node *prev;
    Node(int val)
    {
        this->val = val;
        this->next = NULL;
        this->prev = NULL;
    }
};

class myQueue
{

public:
    Node *head = NULL;
    Node *tail = NULL;

    int countSize = 0;
    void push(int val)
    {
        countSize++;

        Node *newNode = new Node(val);
        if (head == NULL)
        {
            head = newNode;
            tail = newNode;
            return;
        }
        newNode->prev = tail;
        tail->next = newNode;
        tail = tail->next;
    }

    void pop()
    {
        countSize--;

        Node *deleteNode = head;
        head = head->next;
        if (head == NULL)
        {
            tail = NULL;
            delete deleteNode;
            return;
        }
        head->prev = NULL;
    }

    int front()
    {
        return head->val;
    }

    int size()
    {
        return countSize;
    }

    bool empty()
    {
        if (countSize == 0)
            return true;
        else
            return false;
    }
};

int main()
{

    myQueue que;

    int N;
    cin >> N;
    for (int i = 0; i < N; i++)
    {
        int x;
        cin >> x;
        que.push(x);
    }

    while (!que.empty())
    {
        cout << que.front() << endl;
        que.pop();
    }
    return 0;
}