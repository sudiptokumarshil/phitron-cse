#include <bits/stdc++.h>

using namespace std;

int main()
{

    long long int N;
    cin >> N;

    long long int arr[N];

    for (int i = 0; i < N; i++)
    {
        cin >> arr[i];
    }

    bool flag = false;

    for (int i = 0; i < N-1; i++)
    {
        for (int j = i+1; j < N; j++)
        {
            if (arr[i] == arr[j])
            {
               flag = true;
               break;
            }
            
        }
    }

    if (flag == true)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return 0;
}