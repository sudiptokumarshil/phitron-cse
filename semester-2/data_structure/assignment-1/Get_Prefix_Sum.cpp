#include <bits/stdc++.h>

using namespace std;

int main()
{

    long long int N;
    cin >> N;
    long long int A[N];
    long long int B[N];

    for (int i = 0; i < N; i++)
    {
        cin >> A[i];
    }

    B[0] = A[0];

    for (int i = 1; i < N; i++)
    {
        B[i] = A[i] + B[i-1];
    }

    for (int i = N-1; i >= 0; i--)
    {
        cout << B[i] << " ";
    }

    return 0;
}