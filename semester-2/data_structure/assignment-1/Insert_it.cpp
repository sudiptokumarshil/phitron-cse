#include <bits/stdc++.h>

using namespace std;

int main()
{

    int N;
    cin >> N;
    vector<int> A;

    for (int i = 0; i < N; i++)
    {
        int temp;
        cin >> temp;
        A.push_back(temp);
    }

    int M;
    cin >> M;

    vector<int> B;

    for (int i = 0; i < M; i++)
    {
        int temp;
        cin >> temp;
        B.push_back(temp);
    }

    int X;
    cin >> X;

    A.insert(A.begin() + X, B.begin(), B.end());

    for (int i = 0; i < A.size(); i++)
    {
        cout << A[i] << " ";
    }

    return 0;
}