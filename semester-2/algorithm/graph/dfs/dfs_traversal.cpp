#include <bits/stdc++.h>

using namespace std;
const int N = 1e5 + 5;
vector<int> adjaList[N]; // unweighted.
bool visited[N];

void dfs(int u)
{
    // section:1 just afeter entering node u
    visited[u] = true;
    cout << "Visiting Node: " << u << endl;
    for (int v : adjaList[u])
    {
        // section:2 action before entering new child
        if (visited[v])
            continue;
        dfs(v);
        // section:3 action after exiting neighbor.
    }
    // section:4 action before exiting neighbor.
}

int main()
{

    int n, m;
    cin >> n >> m;

    for (int i = 0; i < m; i++)
    {
        int u, v;
        cin >> u >> v;
        adjaList[u].push_back(v); // unweighted.
        adjaList[v].push_back(u); // undirected .
    }

    dfs(1);

    return 0;
}
/*
10 9
1 2
1 3
1 10
2 4
3 5
3 6
4 7
4 8
6 9

*/