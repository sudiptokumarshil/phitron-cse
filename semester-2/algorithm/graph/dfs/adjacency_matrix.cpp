#include <bits/stdc++.h>

using namespace std;
const int N = 1e3 + 5;
int adjaMatrix[N][N];

int main()
{

    int n, m, w;
    cin >> n >> m;

    for (int i = 0; i < m; i++)
    {
        int u, v, w;
        cin >> u >> v >> w;

        /*
        Without weight
        adjaMatrix[u][v] = 1;
        adjaMatrix[v][u] = 1; // Undirected.
        */
       
        // with weight
        adjaMatrix[u][v] = w;
        adjaMatrix[v][u] = w; // Undirected.

    }

    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            cout << adjaMatrix[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}