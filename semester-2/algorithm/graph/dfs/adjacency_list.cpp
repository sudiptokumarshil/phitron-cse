#include <bits/stdc++.h>

using namespace std;
const int N = 1e5 + 5;
// vector<int> adjaList[N]; //unweighted.
vector<pair<int, int>> adjaList[N];

int main()
{

    int n, m;
    cin >> n >> m;

    for (int i = 0; i < m; i++)
    {
        int u, v, w;
        cin >> u >> v >> w;
        // adjaList[u].push_back(v); //unweighted.
        // adjaList[v].push_back(u); //undirected .
        adjaList[u].push_back({v, w});
        // adjaList[v].push_back({u,w}); //undirected .
    }

    for (int i = 1; i <= n; i++)
    {
        cout << "List" << i << ": ";
        for (auto j : adjaList[i])
        {
            cout << "(" << j.first << ", " << j.second << "), ";
        }
        cout << endl;
    }

    return 0;
}

// 4
// 5
// 1 2
// 1 3
// 2 4
// 4 3
// 3 2

/*

4
5
1 2 10
1 3 15
2 4 25
4 3 30
3 2 20
*/