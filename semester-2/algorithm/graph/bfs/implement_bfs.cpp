#include <bits/stdc++.h>

using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}

const int N = 1e5+5;
vector <int> adj[N];
bool visited[N];
int level[N];

void dfs(int s)
{
    queue <int> q;
    q.push(s);
    visited[s] = true;
    level[s] = 0;
    while(!q.empty())
    {
        int u = q.front();
        q.pop();

        cout << "Visiting Node: " << u << endl;

        for(int v : adj[u]) // Get Neighbour of u.
        {   
            if(visited[v]) continue;
            q.push(v);
            visited[v] = true;
            level[v] = level[u] + 1;
        }
    }
}


int main()
{
    init_code();

    int n,m;
    cin >> n >> m;

    for (int i = 0; i < m; i++)
    {
        int u,v;
        cin >> u >> v;

        adj[u].push_back(v);
        adj[v].push_back(u);

    }

    dfs(1);

    cout << endl;

    // Printing Level.
    for(int i =1; i <= n; i++)
    {
        cout << "Level of : " << i <<" -> "<< level[i] << endl; 
    }
}

/*
12 11
1 2
1 3
1 10
2 4
2 5
3 9
10 11
10 12
5 6
5 7
7 8
*/