#include <bits/stdc++.h>

using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}

int parent[8] = {-1,-1,1,1,6,4,-1,-1};

int find(int n)
{
	while(parent[n] != -1)
	{
		n = parent[n];
	}
	return n;
}

int main()
{	
	init_code();

	cout << find(5) <<endl;
	
	return 0;
}