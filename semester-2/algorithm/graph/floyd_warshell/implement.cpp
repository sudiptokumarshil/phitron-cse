#include <bits/stdc++.h>

using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
	    freopen("input.txt", "r", stdin);
	    freopen("output.txt", "w", stdout);
    #endif
}

const int INF = 1e7;

int main()
{	
	init_code();

	int n,e;
	cin >> n >> e;
	int dist[n+1][n+1];
	
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{
			dist[i][j] = INF;
			if (i == j) dist[i][j] = 0;
		}
	}

	while(e--)
	{
		int a,b,w;
		cin >> a >> b >> w;
		dist[a][b] = w;
	}

	cout << "First Print" <<endl;

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{	
			if (dist[i][j] == INF) cout << "INF" << " ";
			else cout << dist[i][j] <<" ";
		}
		cout << endl;
	}



	for (int k = 1; k <= n; k++)
	{
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= n; j++)
			{
				if (dist[i][k]+dist[k][j] < dist[i][j])
				{
					dist[i][j] = dist[i][k]+dist[k][j]; 
				}
			}
		}
	}
	
	cout << endl;
	cout << "Updated Print" <<endl;

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{	
			if (dist[i][j] == INF) cout << "INF" << " ";
			else cout << dist[i][j] <<" ";
		}
		cout << endl;
	}

	return 0;
}