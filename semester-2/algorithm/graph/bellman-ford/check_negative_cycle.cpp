#include <bits/stdc++.h>
using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}

class Edge {

    public: 
        int u;
        int v;
        int w;

        Edge(int a,int b,int w){
            this->u = a;
            this->v = b;
            this->w = w;
        }
};

int main()
{
	init_code();
	
    int n,e;
    cin >> n >> e;
    vector <Edge> v;

    while(e--)
    {
        int a,b,w;
        cin >> a >> b >> w;
        Edge ed(a,b,w);

        v.push_back(ed);
    }

    int dist[n+1];

    for (int i = 0; i < n; i++)
    {
        dist[i] = INT_MAX;    
    }

    dist[1] = 0;

    for (int i = 1; i < n-1; i++)
    {
       for (int j = 0; j < v.size(); j++)
       {
            Edge edge = v[j];
            int a = edge.u;
            int b = edge.v;
            int w = edge.w;

            if (dist[a] + w < dist[b])
            {
                dist[b] = dist[a]+w;   
            }     
       }
    }

    bool cycle = false;

    for (int j = 0; j < v.size(); j++)
    {
            Edge edge = v[j];
            int a = edge.u;
            int b = edge.v;
            int w = edge.w;

            if (dist[a] +w < dist[b])
            {   
                cycle = true;
                break;
                dist[b] = dist[a]+w;   
            }     
    }

    if (cycle)
    {
       cout << "Yes! Cycle Exist." <<endl;
    } else {
        for (int i = 1; i <= n; i++)
        {
            cout << "Node " << i << ": " << dist[i] << endl;
        }
    }
    

	return 0;
}