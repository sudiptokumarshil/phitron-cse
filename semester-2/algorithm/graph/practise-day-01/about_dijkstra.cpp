#include <bits/stdc++.h>
using namespace std;

typedef pair <int,int> pii;
const int N = 1e5+5;
const int INF = 1e9+10;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}

vector <pii> adj[N];
vector <int> dist(N,INF);
vector <bool> visited(N);
int parent[N];

void dijkstra(int source) 
{

	priority_queue <pii ,vector <pii>,greater<pii>> pq; // From smaller.
	dist[source] = 0;
	pq.push({dist[source],source});

	while(!pq.empty())
	{
		int u = pq.top().second;
		pq.pop();
		visited[u] = true;
		
		for (pii vpair: adj[u])
		{
			int v = vpair.first;
			int w = vpair.second;
			if (visited[v]) continue;
			if(dist[v] > dist[u]+w)
			{
				dist[v] = dist[u]+w;
				pq.push({dist[v],v});
				parent[v] = u;
			}

		}
	}

}

int main()
{
	init_code();

	int n,m;
	cin >> n >> m;

	for (int i = 0; i < m; i++)
	{
		int a,b,w;
		cin >> a >> b >>w;
		adj[a].push_back({b,w});
		adj[b].push_back({a,w});
	}

	int source ,destination;
	cin >> source >> destination;

	dijkstra(source);

	for (int i = 1; i <= n; i++)
	{
		cout << "Distance of  Node: " << i <<" ";
		cout <<  dist[i] <<endl;
	}

	// vector <int> path;
    // int current = destination;
    // while(current != -1){
    // 	path.push_back(current);
    // 	current = parent[current];
    // }

    // // reverse(path.begin(), path.end());

    // cout << "Path : ";
    // for (int node : path)
    // {
    // 	cout << node << " ";
    // }
}