#include <bits/stdc++.h>
#define pi pair <int,int> 
using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}
const int N = 1005;
bool visited[N][N];
int distant[N][N];
vector <pi> path = {{-1,0},{1,0},{0,-1},{0,1}};
int n,m;

bool is_valid(int ci,int cj)
{
	if (ci >= 0 && ci < n && cj >= 0 && cj < m) return true;
	return false;
}

void bfs(int si,int sj) 
{
	queue <pi> q;

	q.push({si,sj});
	distant[si][sj] = 0;
	visited[si][sj] = true;

	while(!q.empty())
	{
        pi parent = q.front();
        int pI = parent.first;
        int pJ = parent.second;

        q.pop();

        // Children.
        for (int i = 0; i < 4; i++)
        {
        	pi p = path[i];
        	int cI = pI+p.first;
        	int cJ = pJ+p.second;

        	if (is_valid(cI,cJ) && !visited[cI][cJ])
        	{
        		visited[cI][cJ] = true;
        		q.push({cI,cJ});
        		distant[cI][cJ] = distant[pI][pJ]+1;

        	}

        	// cout << "Children of parent: " << cI << " " << cJ <<endl;
        }
    }
}

int main()
{	
	init_code();

	cin >> n >> m;
	char a[n][m];

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cin >> a[i][j];
		}
	}

	// for (int i = 0; i < n; i++)
	// {
	// 	for (int j = 0; j < m; j++)
	// 	{
	// 		cout <<  [ai][j];
	// 	}
	// 	cout <<endl;
	// }

	int si,sj;
	cin >> si >> sj;

	bfs(si,sj);

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cout <<  distant[i][j] << " ";
		}
		cout <<endl;
	}

	
	return 0;
}