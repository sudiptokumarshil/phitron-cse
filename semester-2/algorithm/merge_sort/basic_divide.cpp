#include <bits/stdc++.h>
using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("../../../input.txt", "r", stdin);
    freopen("../../../output.txt", "w", stdout);
    #endif
}

void divide(int arr[],int l,int r)
{

	for(int i = l; i <= r; i++)
	{
		cout << arr[i] << " ";
	}

	cout << endl;

	if(l < r)
	{
		int mid = (l+r) / 2;
		divide(arr,l,mid);
		divide(arr,mid+1,r);
	}
} 

int main()
{
	init_code();

	int n;
	cin >> n;

	int arr[n];

	for(int i = 0; i < n; i++)
	{
		cin >> arr[i];
	}

	divide(arr,0,n-1);
}