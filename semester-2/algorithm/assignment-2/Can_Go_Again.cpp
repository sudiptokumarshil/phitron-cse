#include <bits/stdc++.h>
using namespace std;

typedef pair<long long int, long long int> pii;
const long long int N = 1e5 + 7;
const long long int INF = 1e18 + 7;
vector<pii> g[N];
vector<pair<pii, long long int>> list_of_edges;
long long int dist[N];
long long int n, m;

bool cycle = false;
void bellman_ford(long long int s)
{
    for (long long int i = 1; i <= n; i++)
        dist[i] = INF;

    dist[s] = 0;

    for (long long int i = 1; i < n; i++)
    {
        for (long long int u = 1; u <= n; u++)
        {
            for (pii vpair : g[u])
            {
                long long int v = vpair.first;
                long long int w = vpair.second;

                if (dist[u] != INF && dist[v] > dist[u] + w)
                {
                    dist[v] = dist[u] + w;
                }
            }
        }
    }

    for (long long int u = 1; u <= n; u++)
    {
        for (pii vpair : g[u])
        {
            long long int v = vpair.first;
            long long int w = vpair.second;

            if (dist[u] != INF && dist[v] > dist[u] + w)
            {
                cycle = true;
            }
        }
    }
}

int main()
{

    cin >> n >> m;

    while (m--)
    {
        long long int u, v, w;
        cin >> u >> v >> w;
        g[u].push_back({v, w});
    }

    long long int s;
    cin >> s;
    bellman_ford(s);

    long long int t;
    cin >> t;

    if (cycle)
    {
        cout << "Negative Cycle Detected" << endl;
    }
    else
    {
        while (t--)
        {
            long long int d;
            cin >> d;
            if (dist[d] == INF)
                cout << "Not Possible" << endl;
            else
                cout << dist[d] << endl;
        }
    }

    return 0;
}