#include <bits/stdc++.h>
using namespace std;

const int INF = 1e7;

int main()
{

    int n, e;
    cin >> n >> e;
    long long dist[n + 1][n + 1];

    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (i == j) dist[i][j] = 0;
            else dist[i][j] = INF;
        }
    }

    while (e--)
    {
        int a, b, w;
        cin >> a >> b >> w;
        dist[a][b] = w;
    }

    for (int k = 1; k <= n; k++)
    {
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (dist[i][k] + dist[k][j] < dist[i][j])
                {
                    dist[i][j] = dist[i][k] + dist[k][j];
                }
            }
        }
    }

    int q;
    cin >> q;

    while (q--)
    {
        int x, y;
        cin >> x >> y;

        if (dist[x][y] == INF)
            cout << -1 << endl;
        else
            cout << dist[x][y] << endl;
    }

    return 0;
}
