#include <bits/stdc++.h>

using namespace std;

const int N = 1e5 + 5;
vector<int> adjaList[N];
bool visited[N];
int level[N];

void bfs(int s)
{
    queue<int> q;
    q.push(s);
    visited[s] = true;
    level[s] = 0;

    while (!q.empty())
    {
        int u = q.front();
        q.pop();
       
        for (int v : adjaList[u])
        {
            if (visited[v])
                continue;
            q.push(v);
            visited[v] = true;
            level[v] = level[u] + 1;
        }
    }
}

int main()
{

    int n, m;
    cin >> n >> m;

    for (int i = 0; i < m; i++)
    {
        int u, v;
        cin >> u >> v;
        adjaList[u].push_back(v);
        adjaList[v].push_back(u);
    }

    int l;
    cin >> l;

    bfs(0);

    vector <int> psLevel;
    for (int i = 0; i < n; i++)
    {
        if (level[i] == l)
        {
            psLevel.push_back(i);
        }
    }

    if (psLevel.empty())
    {
        cout << -1 << endl;
    }
    else
    {
        for (int shop : psLevel)
        {
            cout << shop << " ";
        }
        cout << endl;
    }

    return 0;
}
