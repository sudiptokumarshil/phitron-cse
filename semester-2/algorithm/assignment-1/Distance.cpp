#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 5;
vector<int> adjaList[N];
bool visited[N];
int level[N];

void bfs(int s, int d)
{
    queue<int> q;
    q.push(s);
    visited[s] = true;
    level[s] = 0;

    while (!q.empty())
    {
        int u = q.front();
        q.pop();

        if (u == d)
        {
            cout << level[d] << endl;
            return;
        }

        for (int v : adjaList[u])
        {
            if (!visited[v])
            {
                q.push(v);
                visited[v] = true;
                level[v] = level[u] + 1;
            }
        }
    }
    cout << -1 << endl;
}

int main()
{
    int n, m;
    cin >> n >> m;

    for (int i = 0; i < m; i++)
    {
        int u, v;
        cin >> u >> v;
        adjaList[u].push_back(v);
        adjaList[v].push_back(u);
    }

    int Q;
    cin >> Q;

    for (int i = 0; i < Q; i++)
    {
        int S, D;
        cin >> S >> D;

        for (int j = 0; j < N; j++)
        {
            visited[j] = false;
        }

        bfs(S, D);
    }

    return 0;
}
