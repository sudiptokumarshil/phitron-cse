#include <bits/stdc++.h>

using namespace std;
const int N = 1e5 + 5;
vector<int> adjaList[N];
bool visited[N];
int countNode = 0;

void dfs(int u)
{
    visited[u] = true;
    for (int v : adjaList[u])
    {
        if (visited[v])
            continue;
        if (v == u)
            break;
        countNode++;
        dfs(v);
    }
}

int main()
{
    int N, E;
    cin >> N >> E;

    for (int i = 0; i < E; i++)
    {
        int A, B;
        cin >> A >> B;
        adjaList[A].push_back(B);
        // adjaList[B].push_back(A);
    }

    int K;
    cin >> K;
    dfs(K);

    cout << countNode << endl;

    return 0;
}