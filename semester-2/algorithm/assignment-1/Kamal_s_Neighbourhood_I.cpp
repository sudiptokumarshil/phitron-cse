#include <bits/stdc++.h>

using namespace std;
const int N = 1e5 + 5;

vector<int> adjaList[N];

int main()
{
    int N, E;
    cin >> N >> E;

    for (int i = 0; i < E; i++)
    {
        int A, B;
        cin >> A >> B;
        adjaList[A].push_back(B);
        adjaList[B].push_back(A);
    }

    int K;
    cin >> K;

    cout << adjaList[K].size() << endl;

    return 0;
}