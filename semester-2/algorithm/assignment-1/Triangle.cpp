#include <bits/stdc++.h>

using namespace std;

int main()
{

    int arr[3];
    for (int i = 0; i < 3; i++)
    {
        cin >> arr[i];
    }

    bool isTrue = true;

    for (int i = 0; i < 3 - 1; i++)
    {
        if (arr[i] != arr[i + 1])
        {
            isTrue = false;
            break;
        }
    }

    if (isTrue)
    {
        cout << "Yes" << endl;
    }
    else
    {
        cout << "No" << endl;
    }

    return 0;
}