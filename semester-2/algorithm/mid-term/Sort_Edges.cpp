#include <bits/stdc++.h>
#define pi pair<int, int>

using namespace std;

vector<pi> edges;

bool compare_edges(pi edge_first, pi edge_second)
{
    if (edge_first.first != edge_second.first)
    {
        return edge_first.first < edge_second.first;
    }
    else
    {
        return edge_first.second < edge_second.second;
    }
}

int main()
{
    int E;
    cin >> E;

    for (int i = 0; i < E; i++)
    {
        int A, B;
        cin >> A >> B;
        edges.push_back({A, B});
    }

    sort(edges.begin(), edges.end(), compare_edges);

    for (auto v : edges)
    {
        cout << v.first << " " << v.second << endl;
    }

    return 0;
}
