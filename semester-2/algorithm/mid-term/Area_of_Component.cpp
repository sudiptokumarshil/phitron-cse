#include <bits/stdc++.h>
#define pi pair<int, int>

using namespace std;

int n, m;
const int N = 1000;
bool visited[N][N];
char a[N][N];

vector<pi> path = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

bool is_valid(int ci, int cj)
{
    if (ci >= 0 && ci < n && cj >= 0 && cj < m && a[ci][cj] == '.')
        return true;
    return false;
}

int dfs(int si, int sj)
{
    int cnt = 0;
    visited[si][sj] = true;
    for (int i = 0; i < 4; i++)
    {
        pi p = path[i];
        int cI = si + p.first;
        int cJ = sj + p.second;
        if (is_valid(cI, cJ) && !visited[cI][cJ])
        {
            cnt += dfs(cI, cJ);
        }
    }
    return cnt + 1;
}

int main()
{
    cin >> n >> m;

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cin >> a[i][j];
        }
    }

    vector<int> counted_nodes;

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (!visited[i][j] && a[i][j] == '.')
            {
                int cnt = dfs(i, j);
                counted_nodes.push_back(cnt);
            }
        }
    }

    sort(counted_nodes.begin(), counted_nodes.end());

  
    if(counted_nodes.empty()) cout << "-1" <<endl;
    else cout << counted_nodes[0] <<endl;

    return 0;
}
