#include <bits/stdc++.h>
#define pi pair<int, int>

using namespace std;

int n, m;
const int N = 1000;
bool visited[N][N];
char a[N][N];

vector<pi> path = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

bool is_valid(int ci, int cj)
{
    if (ci >= 0 && ci < n && cj >= 0 && cj < m && a[ci][cj] == '.')
        return true;
    return false;
}

void dfs(int si, int sj)
{
    visited[si][sj] = true;
    for (int i = 0; i < 4; i++)
    {
        pi p = path[i];
        int cI = si + p.first;
        int cJ = sj + p.second;
        if (is_valid(cI, cJ) && !visited[cI][cJ])
        {
            dfs(cI, cJ);
        }
    }
}

int main()
{
    cin >> n >> m;

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cin >> a[i][j];
        }
    }

    int si, sj, di, dj;
    cin >> si >> sj >> di >> dj;

    dfs(si, sj);

    if (visited[di][dj])
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }

    return 0;
}
