#include <bits/stdc++.h>
#define pii pair<int, int>
using namespace std;

const int N = 1005;
bool visited[N][N];
int level[N][N];
vector<pii> path = {{1, 2}, {1, -2}, {-1, 2}, {-1, -2}, {2, 1}, {2, -1}, {-2, 1}, {-2, -1}};
int n, m;

bool is_valid(int ci, int cj)
{
    if (ci >= 0 && ci < n && cj >= 0 && cj < m)
        return true;
    return false;
}

int bfs(int si, int sj, int qi, int qj)
{
    queue<pii> q;

    q.push({si, sj});
    visited[si][sj] = true;
    level[si][sj] = 0;

    while (!q.empty())
    {
        pii parent = q.front();
        int pI = parent.first;
        int pJ = parent.second;

        q.pop();

        if (pI == qi && pJ == qj)
            return level[qi][qj];

        for (int i = 0; i < 8; i++)
        {
            pii p = path[i];
            int cI = pI + p.first;
            int cJ = pJ + p.second;

            if (is_valid(cI, cJ) && !visited[cI][cJ])
            {
                visited[cI][cJ] = true;
                q.push({cI, cJ});
                level[cI][cJ] = level[pI][pJ] + 1;
            }
        }
    }

    return -1;
}

int main()
{
    int T;
    cin >> T;

    for (int k = 0; k < T; k++)
    {
        cin >> n >> m;

        int ki, kj, qi, qj;
        cin >> ki >> kj >> qi >> qj;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                visited[i][j] = false;
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                level[i][j] = 0;
            }
        }

        int steps = bfs(ki, kj, qi, qj);

        cout << steps << endl;
    }

    return 0;
}
