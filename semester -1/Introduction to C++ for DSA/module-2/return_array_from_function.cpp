#include <bits/stdc++.h>

using namespace std;

int *fun()
{

    // int a[5];
    int *a = new int[5];  // a varriable holds heaop memories address.

    for (int i = 0; i < 5; i++)
    {
        cin >> a[i];
    }

    return a;
}

int main()
{

    int *val = fun();

    for (int i = 0; i < 5; i++)
    {
        cout << val[i] << " ";
    }

    return 0;
}