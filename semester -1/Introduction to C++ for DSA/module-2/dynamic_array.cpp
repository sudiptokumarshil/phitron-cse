#include <bits/stdc++.h>

using namespace std;

int main()
{

    int *a = new int[5];

    for (int i = 0; i < 5; i++)
    {
        cin >> a[i];
    }

    for (int i = 0; i < 5; i++)
    {
        cout << a[i] << " ";
    }

    // If we want to delete the array from the heap memory.
    delete[] a;

    return 0;
}