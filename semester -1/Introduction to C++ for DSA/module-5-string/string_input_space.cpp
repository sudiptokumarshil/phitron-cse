#include <bits/stdc++.h>

using namespace std;

int main()
{

    string s;
    getline(cin, s);

    cout << s << endl;

    // But there is a problem.
    
    int n;
    string s;

    cin >> n;
    getline(cin, s);

    cout << n << endl;
    cout << s << endl;

    /*
        if we print both we cannot see any output of the s because pressing enter after the n gitline take enter automatically.
        now we solve this problem in two way.
        1.cin.ingnore() -> it will ignore a character.
        2.getchar() -> it will ignore a character.
    */

    cin >> n;
    cin.ignore();
    getline(cin, s);

    cout << n << endl;
    cout << s << endl;

    return 0;
}