#include <bits/stdc++.h>

using namespace std;

int main()
{

    // Note: Normally we can iterate value from string or array with the help of loop like below.
    string s;
    cin >> s;

    for (int i = 0; i < s.size(); i++)
    {
        cout << s[i] << endl;
    }

    // Same as above we can use built-in pointer of c++
    // There are two built-in pointer in c++ 1.begin 2.end. that are discussed below.
    // cout << s.begin() << endl; // we can use it directly because this is a private property.that is why we have to use derefferencing.
    cout << *s.begin() << endl; // output will be value of first index.
    cout << *s.end() << endl;   // output will be value of last index which is null.
    // Avoiding printing null.
    cout << *(s.end() - 1) << endl;

    // But they are work with iterator.
    string::iterator it;
    for (it = s.begin(); it < s.end(); it++)
    {
        cout << *it << endl;
    }

    // Shortcut.
    for (string::iterator it = s.begin(); it < s.end(); it++)
    {
        cout << *it << endl;
    }

    // in latest compiler.
    for (auto it = s.begin(); it < s.end(); it++)
    {
        cout << *it << endl;
    }
    return 0;
}