#include <bits/stdc++.h>

using namespace std;

int main()
{

    // String is a built-in class of c++.
    string s1 = "Hello world";
    string s2 = "hello world";

    /* 
        We may also reinitialize value of a string like below.
        s1 = "Hello php";
        cout << s1 << endl;
    */

    if (s1 == s2)
    {
        cout << "Same" << endl;
    }
    else
    {
        cout << "Not Same" << endl;
    }

    return 0;
}