#include <bits/stdc++.h>

using namespace std;

int main()
{
    string s;
    cin >> s;
    cout << s << endl;
    // Accessing first index of string.
    cout << s[0] << endl; // important.
    cout << s.at(0) << endl;
    cout << s.front() << endl;
    // Accessing last index of string.
    cout << s.back() << endl; //important.
    cout << s[s.size() - 1] << endl;



    return 0;
}