#include <bits/stdc++.h>

using namespace std;

int main()
{

    string a = "Hello";
    string b = "World";
    // concate two string.
    // a += b;
    cout << a << endl;
    cout << b << endl;
    // a.append(b);
    // add a character or string in the end of the string.
    a.push_back('a');
    // a[5] = 'A'; // it didn't work.
    cout << a << endl;

    // Remove last character of the string.
    a.pop_back();
    cout << a << endl;

    // Assign string.
    a = "gello world";
    cout << a << endl;
    // also we can use built-in function.
    a.assign("Hello php!");
    cout << a << endl;

    // also we can remove character.
    a.erase(4, 2); // starting index,lenth of character.(how many character want to remove).
    cout << a << endl;

    // also we can replace character using replace() method.
    a.replace(4, 2, "so"); // starting index,enth of character,value.

    cout << a << endl;

    return 0;
}