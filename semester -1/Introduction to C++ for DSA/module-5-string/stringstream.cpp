#include <bits/stdc++.h>

using namespace std;

int main()
{

    string s;
    getline(cin, s);

    stringstream ss(s);
    // also we can use like below.
    // ss << s;
    string word;

    // also we can use it.
    // cout << word << endl;
    // cout << word << endl;
    // cout << word << endl;
    // cout << word << endl;

    int count = 0;

    while (ss >> word)
    {
        count++;
        cout << word << endl;
    }
    cout << count << endl;

    return 0;
}