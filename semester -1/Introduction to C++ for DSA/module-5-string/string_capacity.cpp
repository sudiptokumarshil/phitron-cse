#include <bits/stdc++.h>

using namespace std;

int main()
{

    string s = "Hello world";
    // Calculate length of the string without null (\0) character.
    cout << s.size() << endl;
    // Show how many length character can take my machine.
    cout << s.max_size() << endl;

    // capacity method show the capacity of the string.
    cout << s.capacity() << endl;

    // Clear the string using clear() method.

    cout << s << endl;
    s.clear();
    cout << s.size() << endl;

    // empty() Method return true if the string is empty.

    if (s.empty() == true)
    {
        cout << "Yes" << endl;
    }
    else
    {
        cout << "No" << endl;
    }

    // We may resize length of the string.
    cin >> s;
    cout << s << endl;
    s.resize(20, 'S'); // if we want to add extra character.
    cout << s << endl;

    return 0;
}