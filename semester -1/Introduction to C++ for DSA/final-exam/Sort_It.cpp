#include <bits/stdc++.h>

using namespace std;
class Student
{

public:
    string nm;
    int cls;
    char s;
    int id;
    int math_marks;
    int eng_marks;
};

bool compareObj(Student a, Student b)
{
    if ((a.math_marks+a.eng_marks) > (b.math_marks+b.eng_marks))
    {
        return true;
    }
    else if ((a.math_marks+a.eng_marks) == (b.math_marks+b.eng_marks))
    {
        if (a.id > b.id)
        {
            return false;
        }
        return true;
    }
    else
    {
        return false;
    }
}

int main()
{
    int N;
    cin >> N;

    Student S[N];

    for (int i = 0; i < N; i++)
    {
        cin >> S[i].nm >> S[i].cls >> S[i].s >> S[i].id >> S[i].math_marks >> S[i].eng_marks;
    }

    sort(S, S + N, compareObj);

    for (int i = 0; i < N; i++)
    {
        cout << S[i].nm << " " << S[i].cls << " " << S[i].s << " " << S[i].id << " " << S[i].math_marks << " " << S[i].eng_marks << endl;
    }

    return 0;
}