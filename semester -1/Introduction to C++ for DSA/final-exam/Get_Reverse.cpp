#include <bits/stdc++.h>

using namespace std;

class Student
{

public:
    string nm;
    int cls;
    char s;
    int math_marks;
    int eng_marks;
};

int main()
{

    int N;
    cin >> N;

    Student S[N];

    for (int i = 0; i < N; i++)
    {
        cin >> S[i].nm >> S[i].cls >> S[i].s >> S[i].math_marks >> S[i].eng_marks;
    }

    for (int i = N-1; i >= 0; i--)
    {
        cout << S[i].nm << " " << S[i].cls << " " << S[i].s << " " << S[i].math_marks << " " << S[i].eng_marks << endl;
    }

    return 0;
}