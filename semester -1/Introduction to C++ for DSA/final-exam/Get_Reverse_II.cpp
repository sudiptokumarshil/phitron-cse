#include <bits/stdc++.h>

using namespace std;
class Student
{

public:
    string nm;
    int cls;
    char s;
    int id;
};

int main()
{
    int N;
    cin >> N;

    Student S[N];

    for (int i = 0; i < N; i++)
    {
        cin >> S[i].nm >> S[i].cls >> S[i].s >> S[i].id;
    }

    int flag = N-1;
    for (int i = 0; i < N / 2; i++)
    {
        int temp = S[i].id;
        S[i].id = S[flag-i].id;
        S[flag-i].id = temp;
    }

    for (int i = 0; i < N; i++)
    {
        cout << S[i].nm << " " << S[i].cls << " " << S[i].s << " " << S[i].id << " " << endl;
    }

    return 0;
}