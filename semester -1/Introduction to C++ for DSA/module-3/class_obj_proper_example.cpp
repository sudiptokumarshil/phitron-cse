#include <bits/stdc++.h>

using namespace std;

class Person
{
public:
    char name[100];
    float height;
    int age;

    Person(char *n, float h, int ag)
    {
        strcpy(name, n);
        height = h;
        age = ag;
    }
};

int main()
{
    char TaylorName[100] = "Taylor Otwel";

    Person *TaylorOtwel = new Person(TaylorName, 7.2, 40);

    char EvanSName[100] = "Evan You";

    Person *EvanYou = new Person(EvanSName, 7.3, 35);

    if ((TaylorOtwel->age) > (EvanYou->age))
    {
        cout << TaylorOtwel->name << endl;
    }
    else if ((EvanYou->age) > (TaylorOtwel->age))
    {
        cout << EvanYou->name << endl;
    }
    else
    {
        cout << "They are same age" << endl;
    }

    return 0;
}