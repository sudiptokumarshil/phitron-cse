#include <bits/stdc++.h>

using namespace std;

class Person
{
public:
    char name[100];
    float height;
    int age;

    Person(char *n, float h, int ag)
    {
        strcpy(name, n);
        height = h;
        age = ag;
    }
};

int main()
{

    char name[100] = "Tayrol Otwel";
    Person *person = new Person(name, 7.2, 35);
    cout << "Name: " << person->name << endl;
    cout << "Height: " << person->height << endl;
    cout << "Age: " << person->age << endl;

    return 0;
}