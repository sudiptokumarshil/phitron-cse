#include <bits/stdc++.h>

using namespace std;

class Student
{

public:
    char name[100];
    int roll;

    Student(char *n, int r)
    {
        strcpy(name, n);
        roll = r;
    }
};
int main()
{
    char name[100] = "Hello world";
    Student *stu = new Student(name, 23);

    // It is prety easy.
    // cout << "Name: " << stu->name << endl;
    // cout << "Roll: " << stu->roll << endl;

    // It is too hard.
    cout << "Name: " << (*stu).name << endl;
    cout << "Roll: " << (*stu).roll << endl;

    // We are using dynamic object so that we can delete it from memory.

    delete stu;
    cout << endl;
    // Proof of deletion.
    cout << "Name: " << (*stu).name << endl;
    cout << "Roll: " << (*stu).roll << endl;
    return 0;
}