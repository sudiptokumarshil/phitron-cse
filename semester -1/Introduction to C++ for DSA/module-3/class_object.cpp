#include <bits/stdc++.h>

using namespace std;

class Student
{
    public:
        char name[100];
        int age;
};

int main()
{
    Student s;
    s.age = 10;

    char name[100] = "Sudipto";
    strcpy(s.name, name);

    cout << s.age << endl;
    cout << s.name << endl;

    return 0;
}