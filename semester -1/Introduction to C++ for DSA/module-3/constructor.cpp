#include <bits/stdc++.h>

using namespace std;

class Desktop
{

public:
    char motherBoard[100];
    char processor[100];
    int ram;

    // Constructor of the class.
    Desktop(char* mBoard, char* process, int rm)
    {
        strcpy(motherBoard, mBoard);
        strcpy(processor, process);
        ram = rm;
    };
};

int main()
{

    Desktop desktop("Asus", "Intel core i5", 8);

    cout << "Motherboard:" << desktop.motherBoard << endl;
    cout << "Processor:" << desktop.processor << endl;
    cout << "RAM:" << desktop.ram << endl;

    return 0;
}