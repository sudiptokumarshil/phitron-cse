#include <bits/stdc++.h>

using namespace std;

class Student
{

public:
    char name[100];
    int roll;

    Student(char *n, int r)
    {
        strcpy(name, n);
        roll = r;
    }
};

Student information()
{
    char name[100] = "Evan You";
    Student student(name, 38);

    return student;
}

int main()
{

    Student student = information();
    cout << "Name: " << student.name << endl;
    cout << "Roll: " << student.roll << endl;
    return 0;
}