#include <stdio.h>
int main()
{
    char arr[6];
    scanf("%s", arr); // instead of loop.
    printf("%s \n", arr);
    return 0;
}

/*
    Note: but here are some problems in input using scanf.
    if you enter input like this 'sudipto kumar shil',
    you may see output only sudipto,
    because scanf function will understand to close input after getting space.
*/