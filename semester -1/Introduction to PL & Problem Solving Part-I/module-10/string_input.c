#include <stdio.h>
#include <string.h>


int main()
{

    char arr[5];

    for (int i = 0; i < 5; i++)
    {
        scanf("%c", &arr[i]);
    }

    for (int i = 0; i < 5; i++)
    {
        printf("%c \n", arr[i]);
        // printf("%d \n", arr[i]); if you use %d instead of %c, you can see ascii value of a character.
    }


    return 0;
}

/*

    What is string?
    Answer: String is an array of characters but it has a super power.

    Note: Space is also a character.

*/
