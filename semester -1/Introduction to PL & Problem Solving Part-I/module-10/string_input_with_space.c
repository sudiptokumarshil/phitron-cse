#include <stdio.h>
#include <string.h>

int main()
{
    char arr[100];
    /*
        gets Mehtod is not standard,Because some compiler do not support it.
        gets(arr);
        printf("%s", arr);
    */
    fgets(arr, 21, stdin); // this method also take enter as an input.
    printf("%s",arr);
    return 0;
}
