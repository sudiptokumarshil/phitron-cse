#include <stdio.h>

int main()
{
    int arr[5] = {10, 20, 30, 40, 50};
    int sizeInByte = sizeof(arr);
    int sizeOfInteger = sizeof(int);
    int sizeOfArray = sizeof(arr) / sizeof(int);

    printf("Size in byte:%d \n", sizeInByte);
    printf("Size of integer:%d\n", sizeOfInteger);
    printf("Size of array:%d\n", sizeOfArray);

    return 0;
}
