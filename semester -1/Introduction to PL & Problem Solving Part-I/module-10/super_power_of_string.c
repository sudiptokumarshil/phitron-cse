#include <stdio.h>
int main()
{   
    // char arr[6] = {'R','a','h','a','t','\0'}, instead of below.
    char arr[5] = "Rahat"; // We do not need to initialize like an array.
    printf("%s\n",arr); // we can print by %s instead of loop.

    return 0;
}
// Note: By default c compiler set a null '\0' in the end of string.just we need to give space for this null.