#include <stdio.h>
#include <string.h>

int main()
{
    char arr[100];
    // Using raw.
    fgets(arr, 50, stdin);

    /*
    int count = 0;

        for (int i = 0; arr[i] != '\0'; i++)
        {
            count++;
        }
        // Using while loop
        int i = 0;
        while (arr[i] != '\0')
        {
            count++;
            i++;
        }

        printf("%d", count);

    */
    //    Now we can get the same output using built-in function named strlen like the follwing.
    int length = strlen(arr);
    printf("%d", length);

    return 0;
}
