#include <stdio.h>
int main()
{
    int N;

    scanf("%d", &N);

    for (int i = 0; i < N; i++)
    {
        int middle = N / 2;
        if (middle == i)
        {
            for (int j = 0; j < N; j++)
            {

                printf("#");
            }
        }
        else
        {
            for (int j = 0; j < N; j++)
            {

                if (middle == j)
                {
                    printf("#");
                }
                else
                {
                    printf("*");
                }
            }
        }

        printf("\n");
    }

    return 0;
}
