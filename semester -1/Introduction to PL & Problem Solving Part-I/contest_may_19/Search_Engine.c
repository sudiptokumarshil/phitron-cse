#include <stdio.h>
int main()
{
    int T;
    scanf("%d", &T);

    for (int i = 1; i <= T; i++)
    {
        int N, S;
        scanf("%d", &N);

        int arr[N];

        for (int j = 0; j < N; j++)
        {
            scanf("%d", &arr[j]);
        }

        scanf("%d", &S);

        int position = 0;

        for (int k = 0; k < N; k++)
        {
            if (arr[k] == S)
            {
                position = k + 1;
                break;
            }
        }

        if (position > 0)
        {
            printf("Case %d: %d", i, position);
        }
        else
        {
            printf("Case %d: Not Found", i);
        }

        printf("\n");
    }

    return 0;
}
