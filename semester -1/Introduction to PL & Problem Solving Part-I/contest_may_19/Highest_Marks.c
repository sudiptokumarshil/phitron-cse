#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);

    int arr[N];
    int tempArr[N];

    for (int i = 0; i < N; i++)
    {
        scanf("%d", &arr[i]);
        tempArr[i] = arr[i];
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = i + 1; j < N; j++)
        {
            if (tempArr[i] > tempArr[j])
            {
                int temp = tempArr[i];
                tempArr[i] = tempArr[j];
                tempArr[j] = temp;
            }
        }
    }

    int highestMarks = tempArr[N - 1];

    for (int i = 0; i < N; i++)
    {
        printf("%d ", (highestMarks - arr[i]));
    }

    return 0;
}
