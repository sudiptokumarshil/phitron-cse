#include <stdio.h>
int main()
{

    int a, b;
    scanf("%d %d", &a, &b);

    int sum = a + b;

    int arr[sum];
    int inc = 0;
    for (int i = 1; i <= sum; i++)
    {
        arr[inc] = i;
        inc++;
    }

    int evenCount = 0;
    int oddCount = 0;

    for (int i = 0; i < sum; i++)
    {
        if (arr[i] % 2 == 1)
        {
            oddCount++;
        }
        else
        {
            evenCount++;
        }
        // printf("%d ", arr[i]);
    }

    if (oddCount == a && evenCount == b)
    {
        printf("YES\n");
    }
    else
    {
        printf("NO\n");
    }

    return 0;
}
