#include <stdio.h>

int main(){
    int karim,rahim;
    char p;

    // scanf("%d",&karim);
    // printf("%d",karim);

    // If a user want to take user input with % sign ?
    // There are two ways to do this ...
    // 1. Input as a character ... 
    // 2. we can say to scanf function to take input but donot need to store it anywhere...

    // First way -> Input as a character ...  

    // scanf("%d%c %d%c",&karim,&p,&rahim,&p);
    // printf("%d%c %d%c",karim,p,rahim,p);

    // Second way -> we can say to scanf function to take input but donot need to store it anywhere ... 

    scanf("%d%% %d%%",&karim,&rahim);
    printf("%d%% %d%%",karim,rahim);



    return 0;
}