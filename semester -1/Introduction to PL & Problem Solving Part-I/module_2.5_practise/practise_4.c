/*
    You need to take one integer value as input and tell if the value is positive or negative or zero.
    See the sample input and output for more clarification.

*/

#include <stdio.h>

int main()
{

    int userInput;
    scanf("%d", &userInput);

    if (userInput == 0)
    {
        printf("zero \n");
    }
    else if (userInput > 0)
    {
        printf("positive \n");
    }
    else
    {
        printf("negative \n");
    }
    return 0;
}