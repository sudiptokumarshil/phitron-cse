/*

You need to take one non-negative integer (0 or greater than 0) value as input and tell if it is even or odd.
See the sample input and output for more clarification.

*/

#include <stdio.h>

int main()
{

    int userInput;
    scanf("%d", &userInput);
    if (userInput > 0)
    {
        if (userInput % 2 == 0)
        {
            printf("Even \n");
        }
        else
        {
            printf("Odd \n");
        }
    }
    else
    {
        printf("Please enter a Non-negative integer");
    }
    return 0;
}