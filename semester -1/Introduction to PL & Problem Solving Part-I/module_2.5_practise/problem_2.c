/*

    You need to take two integer values as input and show the summation, subtraction, multiplication and division in the given format below.

    For example:
    Inputs are 5 and 2
    Then you’ll give output as:
    5 + 2 = 7
    5 - 2 = 3
    5 * 2 = 10
    5 / 2 = 2.50

*/

#include <stdio.h>

int main()
{

    int first, second;

    scanf("%d %d", &first, &second);

    int add = first + second;
    int sub = first - second;
    int mul = first * second;
    float div = (float)first / (float)second;

    printf("%d + %d = %d\n", first, second, add);
    printf("%d - %d = %d\n", first, second, sub);
    printf("%d * %d = %d\n", first, second, mul);
    printf("%d / %d = %0.2f\n", first, second, div);

    return 0;
}