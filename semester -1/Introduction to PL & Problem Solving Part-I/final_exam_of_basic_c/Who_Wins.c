#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);

    int tiger = 0;
    int pathan = 0;

    for (int i = 0; i < N; i++)
    {
        int X1, X2;
        scanf("%d", &X1);
        scanf("%d", &X2);

        if (X1 > X2)
        {
            tiger++;
        }
        else if (X1 < X2)
        {
            pathan++;
        }
    }

    if (tiger == pathan)
    {
        printf("Draw \n");
    }
    else if (tiger > pathan)
    {
        printf("Tiger \n");
    }
    else
    {
        printf("Pathan \n");
    }

    return 0;
}