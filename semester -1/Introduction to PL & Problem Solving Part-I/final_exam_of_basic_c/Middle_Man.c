#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);

    int arr[N];

    for (int i = 0; i < N; i++)
    {
        scanf("%d", &arr[i]);
    }

    for (int i = 0; i < N - 1; i++)
    {
        for (int j = i + 1; j < N; j++)
        {
            if (arr[i] > arr[j])
            {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }

    if (N % 2 == 0)
    {
        int firstPerson = N / 2;
        int secondPerson = (N / 2) + 1;
        printf("%d %d", arr[firstPerson-1], arr[secondPerson-1]);
    }
    else
    {
        int middle = (N+1)/2;
        printf("%d", arr[middle-1]);
    }

    return 0;
}
