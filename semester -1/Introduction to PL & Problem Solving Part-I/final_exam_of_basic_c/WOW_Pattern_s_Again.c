#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);
    int s = N - 1;
    int k = 1;
    for (int i = 1; i <= N; i++)
    {
        for (int j = 1; j <= s; j++)
        {
            printf(" ");
        }
        for (int j = 1; j <= k; j++)
        {
            if (i % 2 == 0)
            {
                printf("*");
            }
            else
            {
                printf("^");
            }
        }
        s--;
        k = k + 2;
        printf("\n");
    }

    return 0;
}