#include <stdio.h>
int main()
{
    int T;
    scanf("%d",&T);

    int S[T][4];

    for (int i = 0; i < T; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            scanf("%d",&S[i][j]);
        }
    }
    
    for (int i = 0; i < T; i++)
    {   
        int sum = 0;
        for (int j = 1; j < 4; j++)
        {
            // printf("%d ",S[i][j]);
            sum += S[i][j];
        }
        printf("%d",S[i][0]-sum);
        printf("\n");
    }
    
    return 0;
}
