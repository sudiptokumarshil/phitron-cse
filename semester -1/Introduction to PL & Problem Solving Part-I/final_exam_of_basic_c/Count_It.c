#include <stdio.h>
#include <string.h>

int main()
{
    char S[1001];
    fgets(S, 1001, stdin);

    int length = strlen(S);

    int capital = 0;
    int small = 0;
    int space = 0;

    for (int i = 0; i < length; i++)
    {
        if (S[i] < 'a' && S[i] > ' ')
        {
            capital++;
        }
        else if (S[i] == ' ')
        {
            space++;
        }
        else
        {
            small++;
        }
    }

    printf("Capital - %d \n", capital);
    printf("Small - %d \n", small);
    printf("Spaces - %d \n", space);

    return 0;
}
