#include <stdio.h>
int main()
{
    int n, k = 1;
    scanf("%d", &n);

    // First way.
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= i; j++)
        {
            printf("*");
        }
        printf("\n");
    }

    // Second Way.
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= k; j++)
        {
            printf("*");
                }
        k++;

        printf("\n");
    }

    return 0;
}

// 