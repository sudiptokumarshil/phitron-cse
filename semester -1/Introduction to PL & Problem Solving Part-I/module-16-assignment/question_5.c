#include <stdio.h>
#include <string.h>

int is_palindrome(char arr[])
{
    for (int i = 0; i < strlen(arr) / 2; i++)
    {
        if (arr[i] == arr[strlen(arr) - i - 1])
        {
            return 1;
        }
    }
    return 0;
}

int main()
{
    char arr[11];
    scanf("%s", arr);

    if (is_palindrome(arr) == 1)
    {
        printf("Palindrome \n");
    }
    else
    {
        printf("Not Palindrome \n");
    }
    
    return 0;
}
