#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int sumOfTwoNumbers(int num1, int num2)
{
    int sum = num1 + num2;
    return sum;
}

int generateRandomNumber(void)
{
    srand(time(0));
    int randomNumber = rand() % 5;
    return randomNumber;
}

void getSquareOfNumber(int num)
{
    int square = num * num;
    printf("Square of a number %d\n",square);
}

void sayWelcome(void)
{
    printf("Welcome to phitron CSE \n");
}

int main()
{
    printf("Sum of two numbers %d\n", sumOfTwoNumbers(10, 20));
    printf("Generating a random number %d\n", generateRandomNumber());
    getSquareOfNumber(50);
    sayWelcome();
    return 0;
}
