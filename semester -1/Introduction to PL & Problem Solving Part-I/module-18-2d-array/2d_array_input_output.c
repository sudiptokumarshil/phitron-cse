#include <stdio.h>
int main()
{
    printf("\n");

    int arr[5][3];              // first row second column
    for (int i = 0; i < 5; i++) // Based on row
    {
        for (int j = 0; j < 3; j++) // Based on column
        {
            scanf("%d", &arr[i][j]);
            // printf("%d %d \n", arr[i][j]);
            // printf("arr[%d] [%d] ", i, j);
        }
    }

    printf("\n");
    
    for (int i = 0; i < 5; i++) // Based on row
    {
        for (int j = 0; j < 3; j++) // Based on column
        {
            // printf("%d %d \n", arr[i][j]);
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }

    return 0;
}
