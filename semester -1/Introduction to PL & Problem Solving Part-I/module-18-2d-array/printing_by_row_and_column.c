#include <stdio.h>
int main()
{
    int row, col;
    scanf("%d %d", &row, &col);

    int arr[row][col];

    // For getting user input.
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            scanf("%d", &arr[i][j]);
        }
    }

    // printf("Printing 2d array \n");

    // for (int i = 0; i < row; i++)
    // {
    //     for (int j = 0; j < col; j++)
    //     {
    //         printf("%d", arr[i][j]);
    //     }
    //     printf("\n");
    // }

    // For printing exect row wise.
    int exectRow;
    scanf("%d", &exectRow);

    for (int i = 0; i < col; i++)
    {
        printf("%d ", arr[exectRow][i]);
    }
    
    printf("\n");

    // For printing exect column wise.
    int exectCol;
    scanf("%d", &exectCol);

    for (int i = 0; i < row; i++)
    {
        printf("%d ", arr[i][exectCol]);
    }
    printf("\n");

    return 0;
}
