#include <stdio.h>
int main()
{   
    return 0;
}

/*
    Lexicographical comparison means, To compare character by character.
    suppose we have two variables, one is A = Apple and another one is B = Apply.

    now we have to check them one by one. 
    if the first character of A is equal to the first character of B then go forward.
    again check the second index of A is equal to the second index of B ? then go forward.

    But if we do not find equality with one another, we may compare whether A[i] is greater than or less than B[i].
    In Abcd we can find e before y so we can call e less than y.
    That's why, we will consider the apple string to be less than Apply. 
    which means logically like this. A < B;

    Another case is that if have two variables A = abcd B = abc
    then compare between them. if you do not find any inequaluty between them but one variable is finished then you can call it smaller
    example: A = abcd B= abc;
    in this example, B less than A.Because A and B almost equal but B has short length.
    that's why, we will call B is less than A. 

*/