#include <stdio.h>
#include <string.h>

int main()
{
    char A[100],B[100];
    scanf("%s %s",A,B);
    
    for (int i = 0; i <= strlen(B); i++)
    {
        A[i] = B[i];
    }

    printf("%s \n",A);
    
    return 0;
}

/*
    String copy

    Suppose we have two variables. The first one contains A = Apple and the second one contains B = banana.
    now our task is, 'A' will copy the value of 'B' then the output will be A = banana.

    now question is that the 'A' variable contains 5 characters but the 'B' variable contains 6 characters.
    as a result, it should not possible to copy properly.
    so 'A' should be the same as 'B'.

    The same task may be done by a built-in function which is strcpy(A, B);
    Must include string.h header file.
*/