#include <stdio.h>

void func(int *ptr){
    *ptr = 400; //It is called dereferencing ... (We can access and we can change)

    printf("value of ptr %p \n",ptr);
}
int main()
{
    int x = 100;
    func(&x);

    printf("Value of x from the main function %d \n",x);
    printf("Address of x %p \n",&x);
    return 0;
}

/*

    In call by value part, you have passed value from one to another function.
    but in this task, you will pass pointer address from one to another function.

    for better experience, you can follow above code example.


*/