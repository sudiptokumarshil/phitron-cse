#include <stdio.h>

void sum(int x)
{
    x = 200; // This variable completly different from the main function's x;
    // For printing ,memory address of the x variable.
    printf("Address of x from the sum function %p \n", &x);
}

int main()
{
    int x = 100;
    sum(x);

    printf("Value of x from main function %d \n", x);
    printf("Address of x from the main function %p \n", &x);

    return 0;
}

/*
    Call by reference means,passing value into function instead of variable.
    Suppose, you have a variable int x =100 and you have a function named sum(int x).
    if you pass this x into function sum () as a parameter, you are passing value 10 not the variable int x.
    Remember that your int x in main function is totally different from sum() function's variable x.
    For real example, you can follow above code.

*/
