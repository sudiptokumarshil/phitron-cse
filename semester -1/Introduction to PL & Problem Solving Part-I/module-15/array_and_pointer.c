#include <stdio.h>
int main()
{
    int ar[5] = {10, 20, 30, 40, 50};

    // Now we want to see memory address of the ar - > index 0.
    printf("address of ar %p \n", &ar[0]);
    // Magic
    printf("address of ar %p \n", ar); // you will see same as above.

    // Remember that, array is also a pointer that hold memory address of 0 index.
    // Proof
    printf("Value of 0 index %d \n", ar[0]);
    printf("Value of 0 index %d \n", *ar); // By  dereferencing

    // If we want to get value of next index we may do something like below.
    printf("Value of 1 index %d \n", ar[1]);

    // But if we want to get value of next index using dereference. we may do something like below.
    printf("Value of 1 index %d \n", *(ar+1));


    return 0;
}

/*
    Remember that, array is also a pointer that hold memory address of 0 index.

*/
