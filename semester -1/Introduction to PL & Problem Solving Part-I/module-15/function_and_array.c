#include <stdio.h>
int main()
{
    
    return 0;
}

/*
    we will see how we can pass array into function.
    Remember that, we cannot return array from function because we are using static array,
    because after executing function all value of the static array will be removed from memory.
    later in c ++ course, we will learn how to pass array from function.

*/
