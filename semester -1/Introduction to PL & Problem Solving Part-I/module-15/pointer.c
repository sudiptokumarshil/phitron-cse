#include <stdio.h>

int main()
{
    int x = 100;
    int *a = &x;        // We have stored memory address of x variabe.
    printf("%p \n", a); // we have printed value of pointer variable that stored memory address of a.
    printf("%d \n", x);
    *a = 500; // Dereferencing.

    /*
        that's mean, x = 200 and *ptr = 200 both are same.
        Because ptr pointer set the memory location of the x variable.
    */
    printf("%d \n", *a);
    return 0;

    /*
        Note: if you have set int type of a variable, you have to set pointer variable;s datatype int too.
        Mistaken here ().
        int val = 150;
        double *ptr = &val;

        printf("%p",ptr);
    */
}

/*
    Pointer means a memory address of another variable.
    Pointer is a variable that stores memory address of another variable.

    Note: if you have set int type of a variable, you have to set pointer variable;s datatype int too.
*/
