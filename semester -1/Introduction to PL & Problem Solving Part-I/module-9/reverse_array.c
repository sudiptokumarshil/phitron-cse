#include <stdio.h>
int main()
{
    int n;
    printf("Enter a digit for the array:");
    scanf("%d", &n);

    int arr[n];
    printf("Enter all values for the array:");

    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }

    int i = 0, j = n - 1;

    while (i < j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
        i++;
        j--;
    }

    printf("Final result of the reversing array:");
    
    for (int i = 0; i < n; i++)
    {
        printf("%d ", arr[i]);
    }
    return 0;
}

// Note : Two pointers technique ...
// Note: Swap an array ...