#include <stdio.h>

int main()
{
    int n;
    int pos,value;
    printf("Please Enter an Array Length:");
    scanf("%d", &n);
    int arr[n + 1];
    printf("Please Enter an Array:");
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }

    printf("Please enter an array position and a value:");
    scanf("%d %d", &pos,&value);

    for (int i = n; i >= pos + 1; i--)
    {
        arr[i] = arr[i - 1];
    }
    arr[pos] = value;
    for (int i = 0; i <= n; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return 0;
}

/*
    Suppose I have an array of 7 indexes.
    Now I wish to insert a new element in that array containing 7 indexes.
    I want to insert a new element in the 1-number index.
    So I have to do one thing. I can move those elements from left to right.

    Example:  my inserting element will be 100 in the 1-number index.

*/