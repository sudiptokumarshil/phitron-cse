#include <stdio.h>

int main()
{
    int n;
    printf("Enter a digit for the first array length:");
    scanf("%d", &n);

    int a[n];
    printf("Enter all values for the first array:");

    for (int i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }

    int m;
    printf("Enter a digit for the second array length:");
    scanf("%d", &m);

    int b[m];
    printf("Enter all values for the second array:");

    for (int i = 0; i < m; i++)
    {
        scanf("%d", &b[i]);
    }

    int arr[n + m];

    for (int i = 0; i < n; i++)
    {
        arr[i] = a[i];
    }

    int i = n;

    for (int j = 0; j < m; j++)
    {
        arr[i] = b[j];
        i++;
    }

    printf("\n");

    printf("Result of the following copied array:");

    for (int i = 0; i < n + m; i++)
    {
        printf("%d ", arr[i]);
    }

    printf("\n");
    return 0;
}
