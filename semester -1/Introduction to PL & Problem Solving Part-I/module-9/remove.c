#include <stdio.h>
int main()
{
    int N;
    printf("Enter a digit for the array:");
    scanf("%d", &N);

    int arr[N];
    printf("Enter all values for the array:");

    for (int i = 0; i < N; i++)
    {
        scanf("%d", &arr[i]);
    }

    int pos;
    printf("Enter values for the removing position:");
    scanf("%d", &pos);

    for (int i = pos; i < N - 1; i++)
    {
        arr[i] = arr[i + 1];
    }

    printf("Final result of the array:");

    for (int i = 0; i < N - 1; i++)
    {
        printf("%d ", arr[i]);
    }

    printf("\n");

    return 0;
}
