#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);

    long long int A[N];
    long long int sum = 0;
    for (int i = 0; i < N; i++)
    {
        scanf("%lld", &A[i]);
    }

    for (int i = 0; i < N; i++)
    {
        sum += A[i];
    }

    if(sum < 0){
        sum  = sum * -1;
    }
    printf("%lld\n", sum);

    return 0;
}

/*
    Given a number N and an array A of N numbers. Print the absolute summation of these numbers.absolute value : means to remove any negative sign in front of a number .

*/