#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);
    if (N <= 0)
    {
        for (int i = N; i <= 1; i++)
        {
            printf("%d ", i);
        }
    }
    else
    {
        for (int i = 1; i <= N; i++)
        {
            printf("%d ", i);
        }
    }

    return 0;
}

/*
You will be given an integer N. If N is a negative number or zero print from N to 1, otherwise print from 1 to N.
*/