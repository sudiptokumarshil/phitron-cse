#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);
    int A[N];
    int X;

    for (int i = 0; i < N; i++)
    {
        scanf("%d", &A[i]);
    }

    scanf("%d", &X);

    for (int i = 0; i < N; i++)
    {
        if (A[i] == X)
        {
            printf("%d", i);
            break;
        }
        else
        {
            if (sizeof(A) / sizeof(A[0]) - 1 == i)
            {
                printf("%d", -1);
            }
        }
    }

    return 0;
}
/*
    Given a number N and an array A of N numbers. Determine if the number X exists in array A or not and print its position (0-index).
*/
