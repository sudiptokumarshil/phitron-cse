#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);
    int V[N];

    long long int evenSum = 0;
    long long int oddSum = 0;

    for (int i = 0; i < N; i++)
    {
        scanf("%d", &V[i]);
    }

    for (int i = 0; i < N; i++)
    {
        if (V[i] % 2 == 0)
        {
            evenSum += V[i];
        }
        else
        {
            oddSum += V[i];
        }
    }

    printf("%lld %lld\n", evenSum, oddSum);

    return 0;
}

/*
You will be given a positive integer N and N numbers after that. You need to tell the sum of even numbers and the sum of odd numbers separated by a space.
*/
