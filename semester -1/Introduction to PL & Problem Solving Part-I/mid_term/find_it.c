#include <stdio.h>
int main()
{
    int N;
    scanf("%d", &N);
    int A[N];
    for (int i = 0; i < N; i++)
    {
        scanf("%d", &A[i]);
    }

    int X;
    scanf("%d", &X);

    int count = 0;

    for (int i = 0; i < N; i++)
    {
        if (A[i] == X)
        {
            count++;
        }
    }
    printf("%d \n", count);

    return 0;
}

/*
    Question: You will given an integer array A and the size N. You will also be given an integer value X. You need to tell how many times X was appeared in the array.
*/