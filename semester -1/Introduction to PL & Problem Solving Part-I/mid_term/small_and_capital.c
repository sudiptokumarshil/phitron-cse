#include <stdio.h>
#include <string.h>

int main()
{
    char S[1001];
    scanf("%s", S);

    long long int countCapital = 0;
    long long int countSmall = 0;

    for (int i = 0; i < strlen(S); i++)
    {
        if (S[i] < 'a')
        {
            countCapital++;
        }
        else
        {
            countSmall++;
        }
    }

    printf("%lld %lld \n", countCapital, countSmall);

    return 0;
}

/*
    Problem Statement

You will be given a string S. The string will contain both small and capital English alphabets only. You need to tell how many of them are capital alphabets and how many are small alphabets.

Input Format

    Input will contain only S.

*/