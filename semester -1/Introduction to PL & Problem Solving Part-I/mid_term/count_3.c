#include <stdio.h>
#include <string.h>

int main()
{

    char S[1001];
    scanf("%s", S);
    int countLetter[26] = {0};

    for (int i = 0; i < strlen(S); i++)
    {
        countLetter[S[i] - 'a']++;
    }

    for (int i = 0; i < 26; i++)
    {
        printf("%c - %d\n", i + 'a', countLetter[i]);
    }

    return 0;
}

/*
    You will be given a string S as input. The string will contain only English small alphabets and will not contain any spaces. You need to tell how many time each alphabets from a to z appears.
*/