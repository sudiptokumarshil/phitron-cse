#include <stdio.h>
int main()
{
    long long int M1, M2, D;
    scanf("%lld %lld %lld", &M1, &M2, &D);
    long long int result = (M1 * D) / M2;
    printf("%lld\n", result);
    return 0;
}

/*
    Question:

        Suppose there are M1 workers who can complete a work in D days. Unfortunately, some of them became sick before the start of the work, and now there are M2 workers. Can you tell how many days it will take for them to complete the work?

        Note: If the answer is a floating value, print the integer part of the answer.

        Input will contain three positive integers M1, M2 and D.

*/