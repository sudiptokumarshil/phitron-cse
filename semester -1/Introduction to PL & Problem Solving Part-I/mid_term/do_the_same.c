#include <stdio.h>
int main()
{
    long long int N, K;
    scanf("%lld %lld", &N, &K);

    for (int i = 0; i < K; i++)
    {
        for (int j = 1; j <= N; j++)
        {
            printf("%d ", j);
        }
        printf("\n");
    }

    return 0;
}
/*
    Question: You will be given two positive integer N and K. You need to print from 1 to N, and you need to do this K times.
*/