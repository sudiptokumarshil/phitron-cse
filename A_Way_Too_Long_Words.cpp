#include <bits/stdc++.h>
using namespace std;

int main()
{
    int T;
    cin >> T;

    while (T--)
    {
        string word;
        cin >> word;

        if (word.size() <= 10)
        {
            cout << word << endl;
            continue;
        }

        int count = 0;
        for (int i = 1; i < word.size() - 1; i++) count++;
        
        cout << word[0] << count << word[word.size() - 1] << endl;
    }

    return 0;
}