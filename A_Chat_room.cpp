#include <bits/stdc++.h>
using namespace std;

int main()
{   
    string mainString = "ahhellllloou?";
    string searchString = "hello";

    size_t found = mainString.find(searchString);

    if (found != string::npos) {
        cout << "The substring '" << searchString << "' was found at position " << found << endl;
    } else {
        cout << "The substring '" << searchString << "' was not found in the main string." << endl;
    }
    
    return 0;

}