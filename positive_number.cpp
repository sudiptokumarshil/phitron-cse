#include <bits/stdc++.h>

using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}

int main(){

	init_code();

	float numbers[6] = {};

	for (int i = 0; i < 6; ++i) cin >> numbers[i];
	
	int positive = 0;

	for (int i = 0; i < 6; ++i)
	{
		if(numbers[i] > 0) positive ++;
	}

	cout << positive << " " << "valores positivos" << endl;
	

	return 0;
}