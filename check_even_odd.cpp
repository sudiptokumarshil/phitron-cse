#include <bits/stdc++.h>
using namespace std;

void init_code()
{
    #ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    #endif
}

int main(){

	init_code();
	
	vector <int> numbers;
	
	for (int i = 0; i < 5; i++)
	{	
		int number = 0;
		cin >> number;
		numbers.push_back(number);
	}

	int even = 0,odd = 0 ,possitive = 0,negative = 0;

	for (int i = 0; i < numbers.size(); i++)
	{
		if(numbers[i] % 2 == 0) even++;
		if(numbers[i] % 2 != 0) odd ++;
		if(numbers[i] > 0) possitive++;
		if(numbers[i] < 0) negative++;
	}

	cout << even << " " << "valor(es) par(es)" <<endl;
	cout << odd <<" "<< "valor(es) impar(es)" <<endl;
	cout << possitive << " " << "valor(es) positivo(s)" <<endl;
	cout << negative << " " << "valor(es) negativo(s)" <<endl;

	return 0;
}